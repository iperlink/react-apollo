export const createHelper = ({
  createFn,
  file,
  formWithFields,
  mutationName,
  updateQuerys,
}) => {
  const result = {
    ...formWithFields,
    handleSubmitFn: async ({
      mutate,
      formData,
    }) => {
      const {
        onComplete,
        updateFn,
      } = formWithFields
      const parsed = JSON.parse(formData.formData)
      const parsedFormData = parsed[mutationName]
      const resultCrud = await createFn({
        file,
        mutate,
        mutationName,
        parsedFormData,
        updateFn,
        onComplete,
        updateQuerys,
      })
      return resultCrud
    },
  }
  return result
}

