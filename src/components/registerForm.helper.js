import * as cognito from '../cognito'


export const registerHelper = ({
  createFn,
  file,
  formWithFields,
  mutationName,
  registerFn,
  updateQuerys,
}) => {
  const registerFunction = cognito[registerFn]
  const result = {
    ...formWithFields,
    handleSubmitFn: async ({
      mutate,
      formData,
    }) => {
      const {
        onComplete,
        updateFn,
      } = formWithFields
      const parsed = JSON.parse(formData.formData)
      const parsedFormData = parsed[mutationName]
      const registered = await registerFunction({
        file,
        mutate,
        mutationName,
        parsedFormData,
        updateFn,
        onComplete,
      })
      if (!registered) {
        return registered
      }
      const resultCrud = await createFn({
        file,
        mutate,
        mutationName,
        parsedFormData,
        updateFn,
        onComplete,
        updateQuerys,
      })
      return resultCrud
    },
  }
  return result
}

