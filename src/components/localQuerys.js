import gql from 'graphql-tag'

export const formDataQuery = gql`
{
  formData @client
}
`
export const setFormDataMutation = gql`
    mutation setFormData($formData: String) {
      setFormData(formData: $formData) @client
    }
`