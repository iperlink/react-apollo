import { Components } from './importedComp'

import DataComp from './DataComp.init'
import Mutation from './Mutation.init'

const {
  AppContainer,
  Button,
  Loader,
  Table,
  CardRow,
  Login,
  Wistia,
  GalleryRow,
  Flex,
  Pdf,
  Kml,
} = Components

const components = {
  AppContainer,
  Button,
  Loader,
  Table,
  CardRow,
  Login,
  Wistia,
  GalleryRow,
  Flex,
  Pdf,
  Kml,
  ...DataComp,
  ...Mutation,
}

export default components
