import { graphql, compose } from 'react-apollo'
import _ from 'lodash'

import { Components } from './importedComp'

import mutations from './mutationCreator'
import queryArr from './queryCreator'
import { clientConfig } from '../config.loader'


import {
  createFn,
} from '../utils'

import {
  formDataQuery,
  setFormDataMutation,
} from './localQuerys'

import {
  prepareFormWithSubmit,
  getDataLoadQuery,
  atachdataLoadQuery,
  prepareFields,
} from './mutation.helpers'

const { formArray } = clientConfig


const QUERYS = {
  ...mutations,
  ...queryArr,
}

const { Form } = Components

const MutationComp = () => {
  const mutationArray = _.mapValues(formArray, (form, key) => {
    if (!form) {
      console.error('Form ', key, ' is undefined')
    }
    const {
      create,
      file,
      fields,
      mainMut,
      omitMutation,
      registerFn,
      updateQuerys,
    } = form
    if (!mainMut && !omitMutation) {
      console.error('mainMut is not defined on', key)
    }
    const mutationName = mainMut || ''
    const formWithFields = (fields)
      ? {
        ...form,
        fields: prepareFields(fields, mutationName),
      }
      : form
    const formWithSubmmit = prepareFormWithSubmit({
      create,
      createFn,
      file,
      formWithFields,
      mutationName,
      updateQuerys,
      registerFn,
    })
    const ConnectFormWrapper = props => {
      const connectedForm = Form(props, formWithSubmmit)
      return connectedForm
    }
    const dataLoadQuerys = getDataLoadQuery(fields)
    if (formWithSubmmit) {
      if (omitMutation) {
        const toCompose2 = [
          graphql(setFormDataMutation, { name: 'setFormData' }),
          graphql(formDataQuery, { name: 'formData' }),
        ]
        const connected2 = compose(toCompose2)(ConnectFormWrapper)
        return connected2
      }
      const Q = QUERYS[mutationName]
      if (!Q) {
        console.error(`mutation ${mutationName} is not defined in ${_.keys(QUERYS)}`)
      }
      const toComposePre = [
        graphql(Q, { name: mutationName }),
        graphql(setFormDataMutation, { name: 'setFormData' }),
        graphql(formDataQuery, { name: 'formData' }),
      ]
      const toCompose = atachdataLoadQuery(dataLoadQuerys, QUERYS, toComposePre)
      const connected = compose(toCompose)(ConnectFormWrapper)
      return connected
    }

    return ConnectFormWrapper
  })
  return mutationArray
}

export default MutationComp()
