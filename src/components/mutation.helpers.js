import _ from 'lodash'
import { graphql } from 'react-apollo'

import { createHelper } from './createForm.helper'
import { registerHelper } from './registerForm.helper'
import {
  sendValueToLocalState,
} from '../utils'

export const prepareFormWithSubmit = ({
  create,
  createFn,
  file,
  formWithFields,
  mutationName,
  updateQuerys,
  registerFn,
}) => {
  const formWithSubmmit = create
    ? createHelper({
      createFn,
      file,
      formWithFields,
      mutationName,
      updateQuerys,
    })
    : (registerFn)
      ? registerHelper({
        createFn,
        file,
        formWithFields,
        mutationName,
        registerFn,
        updateQuerys,
      })
      : formWithFields
  return formWithSubmmit
}

export const getDataLoadQuery = fields => {
  const result = _.compact(_.map(fields, f => {
    if (f.type === 'select' && f.config.dataFrom) {
      return f.config.dataFrom.Q
    }
    return null
  }))
  return result
}


export const atachdataLoadQuery = (dataLoadQuerys, QUERYS, toCompose) => {
  const result = (dataLoadQuerys.length > 0)
    ? _.map(dataLoadQuerys, Dl => {
      const DlQ = QUERYS[Dl]
      if (!DlQ) {
        console.error(`query ${Dl} is not defined in ${_.keys(QUERYS)}`)
      }
      const Gql = graphql(
        DlQ,
        { name: Dl },
      )
      return Gql
    })
    : []

  return _.concat(toCompose, result)
}

const empty = () => {}
export const prepareFields = (fields, formName) => {
  const mappedFields = _.map(fields, field => {
    const nextFn = field.onChange || empty
    const type = field.type || 'text'
    const result = {
      ...field,
      onChange: (value, setFormData, data) => {
        sendValueToLocalState(value, setFormData, field.name, data, formName)
        nextFn(value, setFormData, data, formName, field.name)
      },
      type,
    }

    return result
  })

  return mappedFields
}
