import React from 'react'

import {
  Route,
  Switch,
} from 'react-router-dom'

import components from './components'
import { client } from './createApolloClient'
import { clientConfig } from './config.loader'

const {
  routeArr,
  dataSources,
  flexArray,
} = clientConfig

const routeCreator = ({ master, childs }) => {
  const MasterComponent = components[master.component]
  const { Login } = components
  const routes = [
    <Route
      key={master.path}
      exact path={master.path}
      render={props => <MasterComponent
        {...props}
        dataSources={dataSources}
        routeTitle={master.title}
        components={components}
        flexArray={flexArray}
        client={client}
        args={master.args}
        
        />}
      />,

  ]

  const createdRoutes = childs.map(x => {
    const CurrComponent = components[x.component]
    if (!CurrComponent) {
      return <Route
      key={x.path}
      path={x.path}
      render={() =>
      <div>
        Component {x.component} is not defined
        </div>
        }
      />
    }
    return <Route
      key={x.path}
      path={x.path}
      render={props =>
      <div>
        <CurrComponent
          {...props}
          routeTitle={x.title}
          components={components}
          dataSources={dataSources}
          flexArray={flexArray}
          args={x.args}
          client={client}
          />
        </div>
        }
      />
  })
  const r = routes.concat(createdRoutes)
  return r
}
export const Routes = props => {
  const r = routeCreator(routeArr, props)
  return (
    <Switch key='master'>
      {r}
    </Switch>
  )
}

