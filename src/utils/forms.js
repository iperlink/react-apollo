
const sendValueToLocalStatePre = (
  value,
  setFormData,
  field,
  formData,
  formName,
) => {
  const parsed = JSON.parse(formData.formData)
  const newForm = (parsed[formName])
    ? {
      ...parsed[formName],
      [field]: value,
    }
    : { [field]: value }
  const formDataToInsert = {
    ...parsed,
    [formName]: newForm,
  }
  setFormData({
    variables:
      { formData: JSON.stringify(formDataToInsert) },
  })
}

export const sendValueToLocalState = _.debounce(sendValueToLocalStatePre, 500, { maxWait: 1000 })
