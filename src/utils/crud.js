import _ from 'lodash'
import QUERYS from '../components/queryCreator'

const updateCache = ({
  data,
  file,
  mutationName,
  store,
  updateQuery,
}) => {
  const inserted = data[mutationName]
  const updateQueryFormed = QUERYS[updateQuery]
  const originalData = store.readQuery({ query: updateQueryFormed })
  if (originalData[updateQuery]) {
    originalData[updateQuery].splice(0, 0, inserted)
    store.writeQuery({
      query: updateQueryFormed,
      data: originalData,
    })
  } else {
    const key = _.keys(originalData)[0]
    console.error(`${updateQuery} is not in the updated query `)
    console.error(`the key is ${key} `)
    console.error(`check updateQuery.N on ${file}`)
  }
}

export const createFn = async (
  {
    file,
    mutate,
    mutationName,
    parsedFormData,
    updateQuerys,
  },
) => {
  try {
    await mutate({
      variables: parsedFormData,
      update: (store, { data }) => {
        if (updateQuerys) {
          if (data[mutationName]) {
            _.map(updateQuerys, updateQuery => {
              updateCache({
                data,
                file,
                mutationName,
                store,
                updateQuery,
              })
            })
          } else {
            console.error(`${mutationName} is not defined in `, data)
          }
        }
      },
    })
    return true
    // onComplete && onComplete(store, post, description, query)
  } catch (e) {
    console.error(e)
    return false
  }
}
