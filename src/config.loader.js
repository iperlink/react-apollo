import { config } from 'client_config'
import { moduleConfig } from 'splinermann_client_config'
console.log('called')
export const clientConfig = (process.env.NODE_ENV === 'production')
  ? config
  : moduleConfig

