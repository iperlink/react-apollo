
export const resolvers = {
  Mutation: {
    setSelectedName: (_, params, { cache }) => {
      cache.writeData({ data: params })

      return null
    },
    setFormData: (_, { formData }, { cache }) => {
      const data = {
        formData,
      }
      cache.writeData({ data })
      return null
      
    },
  }
}
