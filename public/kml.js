var initMap = () => {
  const map = new google.maps.Map(document.getElementById('map'), {
    center: new google.maps.LatLng(-19.257753, 146.823688),
    zoom: 2,
    mapTypeId: 'terrain'
  })
  console.log(map)
  const kmlLayer = new google.maps.KmlLayer('https://s3.amazonaws.com/secdroneimages/test.kml', {
    suppressInfoWindows: true,
    preserveViewport: false,
    map,
  })
  kmlLayer.addListener('click', event => {
    const content = event.featureData.infoWindowHtml
    const testimonial = document.getElementById('capture')
    testimonial.innerHTML = content
  });
}
